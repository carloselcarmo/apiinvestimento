package br.com.APIInvestimento.controllers;

import br.com.APIInvestimento.DTOs.SimulacaoEntradaDTO;
import br.com.APIInvestimento.DTOs.SimulacaoSaidaDTO;
import br.com.APIInvestimento.models.Investimento;
import br.com.APIInvestimento.services.InvestimentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
public class InvestimentoControllerTeste
{
    @MockBean
    private InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testeCriar() throws Exception
    {
        //Preparação
        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Poupanca");
        investimento.setRendimentoAoMes(0.2);

        Mockito.when(investimentoService.incluir(Mockito.any(Investimento.class))).thenReturn(investimento);

        ObjectMapper mapper = new ObjectMapper();

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .content(mapper.writeValueAsString(investimento))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Poupanca")));

        Mockito.verify(investimentoService, Mockito.times(1)).incluir(Mockito.any(Investimento.class));
    }

    @Test
    public void testarCriarSemNome() throws Exception
    {
        //Preparação
        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("");
        investimento.setRendimentoAoMes(0.2);

        Mockito.when(investimentoService.incluir(Mockito.any(Investimento.class))).thenReturn(investimento);

        ObjectMapper mapper = new ObjectMapper();

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .content(mapper.writeValueAsString(investimento))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());

        Mockito.verify(investimentoService, Mockito.times(0)).incluir(Mockito.any(Investimento.class));
    }

    @Test
    public void testarBuscarTodos() throws Exception
    {
        //Preparação
        Investimento investimento1 = new Investimento();
        investimento1.setId(1);
        investimento1.setNome("Poupança");
        investimento1.setRendimentoAoMes(0.2);

        Investimento investimento2 = new Investimento();
        investimento2.setId(1);
        investimento2.setNome("PIC");
        investimento2.setRendimentoAoMes(0.1);

        List<Investimento> investimentos = new ArrayList<>();
        investimentos.add(investimento1);
        investimentos.add(investimento2);

        Mockito.when(investimentoService.buscartodos()).thenReturn(investimentos);

        ObjectMapper mapper = new ObjectMapper();

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .content(mapper.writeValueAsString(investimentos))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarSimular() throws Exception
    {
        //Preparação
        SimulacaoEntradaDTO simulacaoEntradaDTO = new SimulacaoEntradaDTO();
        simulacaoEntradaDTO.setEmail("a@a.com");
        simulacaoEntradaDTO.setNomeInteressado("Joao da Silva") ;
        simulacaoEntradaDTO.setQuantidadeMeses(6);
        simulacaoEntradaDTO.setValorAplicado(1000.0);

        SimulacaoSaidaDTO simulacaoSaidaDTO = new SimulacaoSaidaDTO();
        simulacaoSaidaDTO.setRendimentoPorMes(0.2);
        simulacaoSaidaDTO.setMontante(29859.84);

        Mockito.when(investimentoService.simular(Mockito.anyInt(), Mockito.any(SimulacaoEntradaDTO.class))).thenReturn(simulacaoSaidaDTO);

        ObjectMapper mapper = new ObjectMapper();

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacao")
                .content(mapper.writeValueAsString(simulacaoEntradaDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.montante", CoreMatchers.equalTo(29859.84)));
    }

    @Test
    public void testarSimularProdutoNaoEncontrado() throws Exception
    {
        //Preparação
        SimulacaoEntradaDTO simulacaoEntradaDTO = new SimulacaoEntradaDTO();
        simulacaoEntradaDTO.setEmail("a@a.com");
        simulacaoEntradaDTO.setNomeInteressado("Joao da Silva") ;
        simulacaoEntradaDTO.setQuantidadeMeses(6);
        simulacaoEntradaDTO.setValorAplicado(1000.0);

        SimulacaoSaidaDTO simulacaoSaidaDTO = new SimulacaoSaidaDTO();
        simulacaoSaidaDTO.setRendimentoPorMes(0.2);
        simulacaoSaidaDTO.setMontante(29859.84);

        Mockito.when(investimentoService.simular(Mockito.anyInt(), Mockito.any(SimulacaoEntradaDTO.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/1/simulacao")
                .content(mapper.writeValueAsString(simulacaoEntradaDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
