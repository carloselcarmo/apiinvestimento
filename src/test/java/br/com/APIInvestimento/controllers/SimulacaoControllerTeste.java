package br.com.APIInvestimento.controllers;

import br.com.APIInvestimento.DTOs.SimulacaoDTO;
import br.com.APIInvestimento.services.SimulacaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTeste {

    @MockBean
    private SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarBuscarTodas() throws Exception
    {
        //Preparação
        SimulacaoDTO simulacao1 = new SimulacaoDTO();
        simulacao1.setInvestimentoId(1);
        simulacao1.setEmail("a@a.com");
        simulacao1.setNomeInteressado("Joao da Silva") ;
        simulacao1.setQuantidadeMeses(12);
        simulacao1.setValorAplicado(1000.0);

        SimulacaoDTO simulacao2 = new SimulacaoDTO();
        simulacao2.setInvestimentoId(2);
        simulacao2.setEmail("a@a.com");
        simulacao2.setNomeInteressado("Joao da Silva") ;
        simulacao2.setQuantidadeMeses(12);
        simulacao2.setValorAplicado(1000.0);

        SimulacaoDTO simulacao3 = new SimulacaoDTO();
        simulacao3.setInvestimentoId(2);
        simulacao3.setEmail("a@a.com");
        simulacao3.setNomeInteressado("Jose da Silva") ;
        simulacao3.setQuantidadeMeses(12);
        simulacao3.setValorAplicado(1000.0);

        List<SimulacaoDTO> simulacoes = new ArrayList<>();
        simulacoes.add(simulacao1);
        simulacoes.add(simulacao2);
        simulacoes.add(simulacao3);

        Mockito.when(simulacaoService.buscartodas()).thenReturn(simulacoes);

        ObjectMapper mapper = new ObjectMapper();

        //Execução + Verificação
        mockMvc.perform(MockMvcRequestBuilders.get("/simulacoes")
                .content(mapper.writeValueAsString(simulacoes))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
