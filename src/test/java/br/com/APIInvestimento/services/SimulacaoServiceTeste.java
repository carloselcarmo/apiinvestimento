package br.com.APIInvestimento.services;

import br.com.APIInvestimento.DTOs.SimulacaoDTO;
import br.com.APIInvestimento.models.Investimento;
import br.com.APIInvestimento.models.Simulacao;
import br.com.APIInvestimento.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class SimulacaoServiceTeste
{
    @MockBean
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    @Test
    public void testarBuscarTodas()
    {
        //Preparação
        Simulacao simulacao1 = new Simulacao();
        simulacao1.setId(1);
        simulacao1.setNomeInteressado("Lucas de Melo");
        simulacao1.setEmail("lucas.melo@teste.com");
        simulacao1.setValorAplicado(5000.0);
        simulacao1.setQuantidadeMeses(12);
        simulacao1.setInvestimento(new Investimento(1, "Poupança", 0.2));

        Simulacao simulacao2 = new Simulacao();
        simulacao2.setId(2);
        simulacao2.setNomeInteressado("Lucas de Melo");
        simulacao2.setEmail("lucas.melo@teste.com");
        simulacao2.setValorAplicado(5000.0);
        simulacao2.setQuantidadeMeses(12);
        simulacao2.setInvestimento(new Investimento(2, "PIC", 0.1));

        List<Simulacao> simulacoes = new ArrayList<>();
        simulacoes.add(simulacao1);
        simulacoes.add(simulacao2);

        Mockito.when(simulacaoRepository.findAll()).thenReturn(simulacoes);

        //Execução
        List<SimulacaoDTO> simulacoesEncontradas = simulacaoService.buscartodas();

        //Verificação
        Assertions.assertEquals(simulacoes.size(), simulacoesEncontradas.size());
        Assertions.assertEquals(simulacoes.get(0).getNomeInteressado(), simulacoesEncontradas.get(0).getNomeInteressado());
        Assertions.assertEquals(simulacoes.get(0).getInvestimento().getId(), simulacoesEncontradas.get(0).getInvestimentoId());
        Assertions.assertEquals(simulacoes.get(1).getNomeInteressado(), simulacoesEncontradas.get(1).getNomeInteressado());
        Assertions.assertEquals(simulacoes.get(1).getInvestimento().getId(), simulacoesEncontradas.get(1).getInvestimentoId());
    }
}