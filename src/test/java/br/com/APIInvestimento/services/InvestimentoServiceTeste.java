package br.com.APIInvestimento.services;

import br.com.APIInvestimento.DTOs.SimulacaoEntradaDTO;
import br.com.APIInvestimento.DTOs.SimulacaoSaidaDTO;
import br.com.APIInvestimento.models.Investimento;
import br.com.APIInvestimento.models.Simulacao;
import br.com.APIInvestimento.repositories.InvestimentoRepository;
import br.com.APIInvestimento.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTeste
{
    @MockBean
    private InvestimentoRepository investimentoRepository;

    @MockBean
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    @Test
    public void testarIncluir()
    {
        //Preparação
        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Poupanca");
        investimento.setRendimentoAoMes(0.2);

        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        //Execução
        Investimento investimentoCriado = investimentoService.incluir(investimento);

        //Verificação
        Assertions.assertEquals(investimento.getId(), investimentoCriado.getId());
    }

    @Test
    public void testarBuscarTodos()
    {
        //Preparação
        Investimento investimento1 = new Investimento();
        investimento1.setId(1);
        investimento1.setNome("Poupanca");
        investimento1.setRendimentoAoMes(0.2);

        Investimento investimento2 = new Investimento();
        investimento2.setId(2);
        investimento2.setNome("PIC");
        investimento2.setRendimentoAoMes(0.1);

        List<Investimento> investimentos = new ArrayList<>();

        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentos);

        //Execução
        Iterable<Investimento> investimentosEncontrados = investimentoService.buscartodos();

        //Verificação
        Assertions.assertEquals(investimentos, investimentosEncontrados);
    }

    @Test
    public void testarSimular()
    {
        //Preparação
        SimulacaoEntradaDTO simulacaoDTO = new SimulacaoEntradaDTO();
        simulacaoDTO.setEmail("a@a.com");
        simulacaoDTO.setNomeInteressado("Joao da Silva");
        simulacaoDTO.setQuantidadeMeses(6);
        simulacaoDTO.setValorAplicado(10000.0);

        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Poupança");
        investimento.setRendimentoAoMes(0.2);
        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Integer idInvestimento = 1;
        Double valorMontanteEsperado = 29859.84;

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).thenReturn(simulacaoDTO.converterParaSimulacao());

        //Execução
        SimulacaoSaidaDTO simulacaoExecutada = investimentoService.simular(idInvestimento, simulacaoDTO);

        //Verificação
        Assertions.assertEquals(valorMontanteEsperado , simulacaoExecutada.getMontante());
        Assertions.assertEquals(investimento.getRendimentoAoMes() , simulacaoExecutada.getRendimentoPorMes());
        Mockito.verify(simulacaoRepository, Mockito.times(1)).save(Mockito.any(Simulacao.class));
    }

    @Test
    public void testarSimularComInvestimentoNaoExistente() throws Exception
    {
        //Preparação
        SimulacaoEntradaDTO simulacaoDTO = new SimulacaoEntradaDTO();
        simulacaoDTO.setEmail("a@a.com");
        simulacaoDTO.setNomeInteressado("Joao da Silva");
        simulacaoDTO.setQuantidadeMeses(6);
        simulacaoDTO.setValorAplicado(10000.0);

        Optional<Investimento> investimentoOptional = Optional.empty();

        Integer idInvestimento = 1;

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).thenReturn(simulacaoDTO.converterParaSimulacao());

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.simular(idInvestimento, simulacaoDTO);});
    }

    @Test
    public void testarSimularComErroAoSalvar() throws Exception
    {
        //Preparação
        SimulacaoEntradaDTO simulacaoDTO = new SimulacaoEntradaDTO();
        simulacaoDTO.setEmail("a@a.com");
        simulacaoDTO.setNomeInteressado("Joao da Silva");
        simulacaoDTO.setQuantidadeMeses(6);
        simulacaoDTO.setValorAplicado(10000.0);

        Investimento investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("Poupança");
        investimento.setRendimentoAoMes(0.2);
        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Integer idInvestimento = 1;

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).thenThrow(RuntimeException.class);

        //Execução + Verificação
        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.simular(idInvestimento, simulacaoDTO);});
    }
}
