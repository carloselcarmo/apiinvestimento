package br.com.APIInvestimento.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    @NotNull(message = "Nome deve ser informado")
    @NotBlank(message = "Nome não pode ser vazio")
    @Size(min=3, message = "Nome deve ter pelo menos 3 caracteres")
    private String nome;

    @NotNull(message = "Rendimento ao mês deve ser informado")
    @Digits(integer = 1, fraction = 6, message = "Rendimento ao mês fora do padrão")
    private Double rendimentoAoMes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(Double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Investimento() {
    }

    public Investimento(int id, String nome, double rendimentoAoMes) {
        this.id = id;
        this.nome = nome;
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Double simular(Double montante, Integer quantidadeMeses)
    {
        for(Integer i = 0; i < quantidadeMeses ; i++)
        {
            montante = montante +  (montante * this.getRendimentoAoMes());
        }

        return BigDecimal.valueOf(montante).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
    }
}
