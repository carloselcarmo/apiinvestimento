package br.com.APIInvestimento.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Simulacao
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message =  "Nome não pode ser vazio")
    @NotNull(message = "Nome deve ser informado")
    @Size(min = 5, message = "O nome deve ter pelo menos 5 caracteres")
    private String nomeInteressado;

    @Email(message = "E-mail é inválido")
    @NotNull(message = "E-mail não pode ser nulo")
    private String email;

    @NotNull (message = "Valor aplicado deve ser informado")
    @DecimalMin(value = "0", message = "Valor aplicado deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor aplicado fora do padrão")
    private Double valorAplicado;

    @NotNull (message = "Quantidade de Meses deve ser informado")
    @DecimalMin(value = "0", message = "Quantidade de Meses deve ser maior ou igual a zero")
    private Integer quantidadeMeses;

    @ManyToOne
    @NotNull (message = "Investimento deve ser informado")
    private Investimento investimento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public Integer getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public Simulacao() {
    }
}