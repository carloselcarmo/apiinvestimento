package br.com.APIInvestimento.repositories;

import br.com.APIInvestimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {

}
