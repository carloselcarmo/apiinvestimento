package br.com.APIInvestimento.repositories;

import br.com.APIInvestimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

}