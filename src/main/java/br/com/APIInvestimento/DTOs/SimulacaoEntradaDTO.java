package br.com.APIInvestimento.DTOs;

import br.com.APIInvestimento.models.Simulacao;

import javax.validation.constraints.*;

public class SimulacaoEntradaDTO
{
    @NotBlank(message =  "Nome não pode ser vazio")
    @NotNull(message = "Nome deve ser informado")
    @Size(min = 5, message = "O nome deve ter pelo menos 5 caracteres")
    private String nomeInteressado;

    @Email(message = "E-mail é inválido")
    @NotNull(message = "E-mail não pode ser nulo")
    private String email;

    @NotNull (message = "Valor aplicado deve ser informado")
    @DecimalMin(value = "0", message = "Valor aplicado deve ser maior ou igual a zero")
    @Digits(integer = 6, fraction = 2, message = "Valor aplicado fora do padrão")
    private Double valorAplicado;

    @NotNull (message = "Quantidade de Meses deve ser informado")
    @DecimalMin(value = "0", message = "Quantidade de Meses deve ser maior ou igual a zero")
    private Integer quantidadeMeses;

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public Integer getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public SimulacaoEntradaDTO() {
    }

    public Simulacao converterParaSimulacao() {
        Simulacao simulacao = new Simulacao();
        simulacao.setValorAplicado(this.valorAplicado);
        simulacao.setQuantidadeMeses(this.quantidadeMeses);
        simulacao.setEmail(this.email);
        simulacao.setNomeInteressado(this.email);
        return simulacao;
    }
}
