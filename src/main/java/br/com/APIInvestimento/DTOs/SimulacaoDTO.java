package br.com.APIInvestimento.DTOs;

import br.com.APIInvestimento.models.Simulacao;

public class SimulacaoDTO
{
    private String nomeInteressado;
    private String email;
    private Double valorAplicado;
    private Integer quantidadeMeses;
    private Integer investimentoId;

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public Integer getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Integer getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(Integer investimentoId) {
        this.investimentoId = investimentoId;
    }

    public SimulacaoDTO() {
    }

    public SimulacaoDTO(Simulacao simulacao)
    {
        this.setValorAplicado(simulacao.getValorAplicado());
        this.setQuantidadeMeses(simulacao.getQuantidadeMeses());
        this.setNomeInteressado(simulacao.getNomeInteressado());
        this.setEmail(simulacao.getEmail());
        this.setInvestimentoId(simulacao.getInvestimento().getId());
    }
}
