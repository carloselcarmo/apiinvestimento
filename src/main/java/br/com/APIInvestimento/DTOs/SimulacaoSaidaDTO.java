package br.com.APIInvestimento.DTOs;

public class SimulacaoSaidaDTO {
    private Double rendimentoPorMes;

    private Double montante;

    public Double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(Double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public Double getMontante() {
        return montante;
    }

    public void setMontante(Double montante) {
        this.montante = montante;
    }

    public SimulacaoSaidaDTO() {
    }
}
