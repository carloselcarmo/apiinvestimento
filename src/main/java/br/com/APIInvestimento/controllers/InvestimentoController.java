package br.com.APIInvestimento.controllers;

import br.com.APIInvestimento.DTOs.SimulacaoEntradaDTO;
import br.com.APIInvestimento.DTOs.SimulacaoSaidaDTO;
import br.com.APIInvestimento.models.Investimento;
import br.com.APIInvestimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController
{
    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento incluir(@RequestBody @Valid Investimento investimento)
    {
        return investimentoService.incluir(investimento);
    }

    @GetMapping
    public Iterable<Investimento> buscarTodos()
    {
        return investimentoService.buscartodos();
    }

    @PostMapping("/{id}/simulacao")
    public SimulacaoSaidaDTO simular(@PathVariable(name = "id") Integer id, @RequestBody @Valid SimulacaoEntradaDTO simulacaoEntradaDTO)
    {
        try
        {
            return investimentoService.simular(id, simulacaoEntradaDTO);
        }
        catch (RuntimeException ex)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
