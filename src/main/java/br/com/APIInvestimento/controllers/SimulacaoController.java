package br.com.APIInvestimento.controllers;

import br.com.APIInvestimento.DTOs.SimulacaoDTO;
import br.com.APIInvestimento.models.Simulacao;
import br.com.APIInvestimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public List<SimulacaoDTO> buscarTodas()
    {
        return simulacaoService.buscartodas();
    }
}
