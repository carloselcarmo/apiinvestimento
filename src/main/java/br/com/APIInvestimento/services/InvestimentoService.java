package br.com.APIInvestimento.services;

import br.com.APIInvestimento.DTOs.SimulacaoEntradaDTO;
import br.com.APIInvestimento.DTOs.SimulacaoSaidaDTO;
import br.com.APIInvestimento.models.Investimento;
import br.com.APIInvestimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class InvestimentoService
{
    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    public Investimento incluir(Investimento investimento)
    {
        return investimentoRepository.save(investimento);
    }

    public Iterable<Investimento> buscartodos()
    {
        return investimentoRepository.findAll();
    }

    public SimulacaoSaidaDTO simular(Integer idInvestimento, SimulacaoEntradaDTO simulacaoEntradaDTO)
    {
        Optional<Investimento> investimento = investimentoRepository.findById(idInvestimento);

        if(investimento.isPresent())
        {
            simulacaoService.incluir(simulacaoEntradaDTO, investimento.get());

            Double montante = investimento.get().simular(simulacaoEntradaDTO.getValorAplicado(), simulacaoEntradaDTO.getQuantidadeMeses());

            Double mediaMensal = (montante - simulacaoEntradaDTO.getValorAplicado()) / simulacaoEntradaDTO.getQuantidadeMeses();

            SimulacaoSaidaDTO simulacaoSaidaDTO =  new SimulacaoSaidaDTO();
            simulacaoSaidaDTO.setMontante(montante);
            simulacaoSaidaDTO.setRendimentoPorMes(BigDecimal.valueOf(mediaMensal).setScale(2, RoundingMode.HALF_EVEN).doubleValue());

            return  simulacaoSaidaDTO;
        }
        else
        {
            throw new RuntimeException("Não existe o investimento informado");
        }
    }
}
