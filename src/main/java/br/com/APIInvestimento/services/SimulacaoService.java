package br.com.APIInvestimento.services;

import br.com.APIInvestimento.DTOs.SimulacaoDTO;
import br.com.APIInvestimento.DTOs.SimulacaoEntradaDTO;
import br.com.APIInvestimento.models.Investimento;
import br.com.APIInvestimento.models.Simulacao;
import br.com.APIInvestimento.repositories.SimulacaoRepository;
import org.hibernate.event.internal.PostDeleteEventListenerStandardImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService
{
    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public List<SimulacaoDTO> buscartodas()
    {
        List<SimulacaoDTO> simulacoes = new ArrayList<>();

        Iterable<Simulacao> listasimulacao = simulacaoRepository.findAll();
        for (Simulacao simulacao : listasimulacao)
        {
            simulacoes.add(new SimulacaoDTO(simulacao));
        }

        return simulacoes;
    }

    public void incluir(SimulacaoEntradaDTO simulacaoEntradaDTO, Investimento investimento)
    {
        Simulacao simulacao = simulacaoEntradaDTO.converterParaSimulacao();
        simulacao.setInvestimento(investimento);
        simulacaoRepository.save(simulacao);
    }
}
